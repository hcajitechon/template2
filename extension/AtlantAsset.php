<?php
namespace itechon\extension;

use yii\base\Exception;
use yii\web\AssetBundle;

/**
 * AtlantAsset
 * @since 0.1
 */
class AtlantAsset extends AssetBundle
{
    /**
     * alias que aponta para a diretoria dist
     * para ser chamado Yii::$app->assetManager->getPublishedUrl(AtlantAsset::ALIAS);
     */
    const ALIAS = '@vendor/itechon/atlant-theme/dist';
    public $sourcePath = self::ALIAS;
    public $css = [
        //'css/jquery/jquery-ui.min.css',
        //'css/bootstrap/bootstrap.min.css',
        'css/fontawesome/font-awesome.min.css',
        'css/summernote/summernote.css',
        'css/codemirror/codemirror.css',
        'css/nvd3/nv.d3.css',
        'css/mcustomscrollbar/jquery.mCustomScrollbar.css',
        'css/fullcalendar/fullcalendar.css',
        'css/blueimp/blueimp-gallery.min.css',
        'css/rickshaw/rickshaw.css',
        'css/dropzone/dropzone.css',
        'css/introjs/introjs.min.css',
        'css/animate/animate.min.css',
        'css/theme-default.css',
    ];
    public $js = [
        'js/plugins/icheck/icheck.min.js',
        'js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js',
        'js/plugins/scrolltotop/scrolltopcontrol.js',
        'js/plugins/morris/raphael-min.js',
        'js/plugins/morris/morris.min.js',
        'js/plugins/rickshaw/d3.v3.js',
        'js/plugins/rickshaw/rickshaw.min.js',
        'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        'js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        'js/plugins/bootstrap/bootstrap-datepicker.js',
        'js/plugins/owl/owl.carousel.min.js',
        'js/plugins/moment.min.js',
        'js/plugins/daterangepicker/daterangepicker.js',
        'js/actions.js',
        'js/plugins.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        //'itechon\extension\ICheckAsset',
        //'itechon\extension\FontAwesomeAsset'
    ];

    public $publishOptions = [
        //'forceCopy' => true,
        //'only' => [
        //    'assets/',
        //    'audio/',
        //    'css/',
        //    'img/',
        //    'js/',
        //]
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
